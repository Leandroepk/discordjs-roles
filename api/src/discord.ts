import { Client, GatewayIntentBits } from "discord.js";
import config from "../config.json";

export class Discord {
  client: Client;
  constructor() {
    const intents = Object.keys(GatewayIntentBits).map((a: any) => {
      return GatewayIntentBits[a];
    });

    this.client = new Client({
      // @ts-ignore
      intents,
    });
    this.client.login(config.token);
  }

  async getRoleByName(guildId: string, roleName: string) {
    const guild = this.client.guilds.resolve(guildId);
    if (guild) {
      const roles = await guild.roles.fetch();
      return roles.find((role) => role.name === roleName);
    }
    return undefined;
  }

  async getMemberById(guildId: string, userId: string) {
    const guild = this.client.guilds.resolve(guildId);
    if (guild) {
      let members = await guild.members.fetch();
      members = members.filter((member) => !member.user.bot);
      return members.find((member) => member.user.id === userId);
    }
    return undefined;
  }
}
