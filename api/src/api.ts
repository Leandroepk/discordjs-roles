import Express, { Request, Response } from "express";
import config from "../config.json";
import cors from "cors";
import { Discord } from "./discord";

export class Api {
  discord: Discord;

  constructor() {
    this.discord = new Discord();
    this.initExpress();
  }

  initExpress() {
    const app = Express();
    const port = 3000;

    app.get("/", (_req, res) => {
      res.send("Hello World!");
    });

    app.get("/login", this.login.bind(this));
    app.get("/logout", this.logout.bind(this));

    app.listen(port, () => {
      console.log(`Example app listening on port ${port}`);
    });

    app.use(
      cors({
        origin: "http://localhost:5173",
      })
    );
  }

  async login(req: Request, res: Response) {
    if (typeof req.query.userId === "string") {
      const user = await this.discord.getMemberById(
        config.server,
        req.query.userId
      );
      const role = await this.discord.getRoleByName(
        config.server,
        config.roleName
      );
      if (!user || !role) {
        res.status(404);
        return;
      }

      await user.roles.add(role);
      res.send(role);
    } else {
      res.status(400).send("Missing user id");
    }
  }

  async logout(req: Request, res: Response) {
    if (typeof req.query.userId === "string") {
      const user = await this.discord.getMemberById(
        config.server,
        req.query.userId
      );
      const role = await this.discord.getRoleByName(
        config.server,
        config.roleName
      );
      if (!user || !role) {
        res.status(404);
        return;
      }

      await user.roles.remove(role);
      res.send(role);
    } else {
      res.status(400).send("Missing user id");
    }
  }
}
