import { createClient, Session } from "@supabase/supabase-js";
import { useEffect, useState } from "react";
import "./App.css";

const supabase = createClient(
  import.meta.env.VITE_SUPABASE_URL,
  import.meta.env.VITE_SUPABASE_SECRET_KEY
);

const api = `${import.meta.env.VITE_API_URL}:${import.meta.env.VITE_API_PORT}`;

function App() {
  const [auth, setAuth] = useState<Session | null>(null);
  const [hasRole, setHasRole] = useState<boolean>(
    Boolean(Number(localStorage.getItem("hasRole")))
  );

  const updateAuth = async () => {
    const {
      data: { session },
    } = await supabase.auth.getSession();
    setAuth(session);
    if (!hasRole && session) {
      addRole(session.user.user_metadata.provider_id);
    }
    if (hasRole && !session) {
    }
  };

  const addRole = async (userId: string) => {
    const url = `${api}/login?userId=${userId}`;
    await fetch(url).then((r) => r.json());
    setHasRole(true);
    localStorage.setItem("hasRole", "1");
  };

  const removeRole = async (userId: string) => {
    const url = `${api}/logout?userId=${userId}`;
    await fetch(url).then((r) => r.json());
    setHasRole(false);
    localStorage.setItem("hasRole", "0");
  };

  async function login() {
    const { error } = await supabase.auth.signInWithOAuth({
      provider: "discord",
    });
    if (error) {
      console.error({ error });
    }
  }

  async function logout() {
    await supabase.auth.signOut();
    if (auth) removeRole(auth.user.user_metadata.provider_id);
    updateAuth();
  }

  useEffect(() => {
    updateAuth();
  }, []);

  return (
    <>
      {!auth ? (
        <button onClick={login}>Vincular</button>
      ) : (
        <button onClick={logout}>Desvincular</button>
      )}
      <h1></h1>
    </>
  );
}

export default App;
