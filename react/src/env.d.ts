/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly URL: string;
  readonly SECRET_KEY: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
